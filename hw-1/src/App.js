
import React, {useState} from "react";



import "./App.css";
import Modal from "./modalComponents/Modal"
import ActionButton from "./modalComponents/ActionButton"
import StartButton from "./modalComponents/StartButton"


function App() {
    const [modalActive, setModalActive] = useState('');

  return (
      <div className="app">

        <main>

            <StartButton text="First" onClick={() => setModalActive("modal-1")} />
            <StartButton text="Second" onClick={() => setModalActive("modal-2")} />

        </main>

            <Modal
                id="modal-1"
                active={modalActive}
                 setActive={setModalActive}
                 header={"modal-window-1"}
                 closeButton={true}
                 text={"TEXT INSIDE MODAL WINDOW 1"}
                   actions={(<div>
                       <ActionButton
                           text={"good morning"}
                           backgroundColor={"#ee432a"}
                           fn={() => alert("hi!")} />
                       <ActionButton
                           text={"cool evening"}
                           fn={() => alert("life!")}
                           backgroundColor={"#ee432a"}/>
                   </div>)} >
            </Modal>

            <Modal id="modal-2"
                   active={modalActive}
                 setActive={setModalActive}
                 header={"modal-window-2"}
                 closeButton={false}
                 text={"TEXT INSIDE MODAL WINDOW 2"}
                 actions={(<div>
                     <ActionButton
                         text={"say Hello"}
                         backgroundColor={"#ae432a"}
                         fn={() => alert("hello!")} />
                     <ActionButton
                         text={"say Goodbye"}
                         fn={() => alert("buy!")}
                         backgroundColor={"#ae432a"}/>
                 </div>)} >
            </Modal>

      </div>
  )
}

export default App;