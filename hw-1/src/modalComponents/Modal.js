import React from "react";
import "./Modal.css"

import CloseButton from "./CloseButton";

const Modal = ({id, active, setActive, header, closeButton, text, actions}) => {
        return (

            <div className={active === id ? "modal active" : "modal"} onClick={() => setActive("")}>

                <div className={active === id ? "modal__content active" : "modal__content"} onClick={e => e.stopPropagation()}>

                    <div className="modal-title">
                        <h1>{header}</h1>
                        <CloseButton isActive={closeButton} onClick={() => setActive("")}/>
                    </div>

                    <div className="modal-text">
                        {text}
                    </div>

                    <div className="modal-actions">
                        {actions}
                    </div>
                </div>

            </div>

        );
};

export default Modal;

