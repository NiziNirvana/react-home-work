import React from "react";
import "./CloseButton.css"

function CloseButton(props) {
    if (props.isActive) {
        return <button className="close-button" onClick={props.onClick}/>;
    }
    return '';
}

export default CloseButton;